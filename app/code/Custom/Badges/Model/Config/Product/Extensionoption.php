<?php
namespace Custom\Badges\Model\Config\Product;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Extensionoption extends AbstractSource
{
    protected $optionFactory;

    public function getAllOptions()
    {
        $this->_options = [];
        $this->_options[] = ['label' => 'New', 'value' => 'badge_new'];
        $this->_options[] = ['label' => 'Sale', 'value' => 'badge_sale'];
        $this->_options[] = ['label' => 'Eco', 'value' => 'badge_eco'];
        $this->_options[] = ['label' => 'Popular', 'value' => 'badge_popular'];
        $this->_options[] = ['label' => 'Best Seller', 'value' => 'badge_best_seller'];
        $this->_options[] = ['label' => 'Limited', 'value' => 'badge_limited'];

        return $this->_options;
    }


}